﻿using System.Collections.Generic;
using UnityEngine;

public class TerrainController : MonoBehaviour
{
    [SerializeField] int locationsCount = 50;
    [SerializeField] GameObject player;
    [SerializeField] GameObject Grass;
    [SerializeField] GameObject Road;
    [SerializeField] GameObject Water;
    [SerializeField] GameObject StartPlatform;
    [SerializeField] Transform positionLocation;
    [SerializeField] GameObject LocationsContainer;
    [SerializeField] GameObject CarContainer;
    private List<GameObject> locationsList;
    private GameObject[] locations;
    private bool isDestroy = false;
    private void Awake()
    {
        locations = new GameObject[] { Grass, Road, Water };
    }
    private void Start()
    {
        CreateTerrain();
    }
    public void CreateTerrain()
    {
        gameObject.SetActive(false);
        if (LocationsContainer.transform.childCount!=0 || CarContainer.transform.childCount!=0)
        {
            for (int i = 0; i < LocationsContainer.transform.childCount; i++)
            {
                Destroy(LocationsContainer.transform.GetChild(i).gameObject);
            }
            for (int i = 0; i < CarContainer.transform.childCount; i++)
            {
                Destroy(CarContainer.transform.GetChild(i).gameObject);
            }
        }
        locationsList = new List<GameObject>();
        Instantiate(StartPlatform, new Vector3(0, -1, -3.25f), Quaternion.identity, LocationsContainer.transform);
        for (int i = 0; i < locationsCount; i++)
        {
            locationsList.Add(locations[i % (locations.Length)]);
        }
        locationsList = Shuffle(locationsList);
        for (int i = 0; i < locationsCount; i++)
        {
            positionLocation.position = new Vector3(0, -1, i);
            Instantiate(locationsList[i], positionLocation.position, Quaternion.identity, LocationsContainer.transform);
        }
        player.SetActive(true);
        player.transform.position = new Vector3(0, 0, -1);
    }
    private void Update()
    {
        if (isDestroy)
        {
            locationsList.RemoveAt(0);
            isDestroy = false;
            AddTerrain();
        }
    }
    private void AddTerrain()
    {
        positionLocation.position = new Vector3(0, -1, positionLocation.position.z + 1);
        GameObject currentLoaction = Instantiate(locations[Random.Range(0, locations.Length)], positionLocation.position, Quaternion.identity, LocationsContainer.transform);
        locationsList.Add(currentLoaction);
    }
    private void OnTriggerEnter(Collider other)
    {
        isDestroy = true;
        Destroy(other.gameObject);
    }
    private static List<GameObject> Shuffle(List<GameObject>list)
    {
        for(int i = 0; i<list.Count; i++)
        {
            GameObject currentValue = list[i];
            int randomIndex = Random.Range(i, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = currentValue;
        }
        return list;
    }
}
