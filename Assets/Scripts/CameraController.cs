﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Transform player;
    [SerializeField] Vector3 destination;
    private void Update()
    {
        Vector3 currentPos = player.position + destination;
        transform.position = Vector3.Lerp(transform.position, currentPos, Time.deltaTime);
    }
}
