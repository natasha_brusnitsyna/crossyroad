﻿using UnityEngine;
public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] float leftLimit;
    [SerializeField] float rightLimit;
    [SerializeField] float downLimit;
    private Vector3 startPosition, endPosition;
    private Touch touch;
    private AudioSource audio;
    private GameObject GameOver;
    private GameObject Destroyer;
    private void Start()
    {
        audio = GetComponent<AudioSource>();
        GameOver = GameObject.FindGameObjectWithTag("GameOver");
        Destroyer = GameObject.FindGameObjectWithTag("Destroyer");
    }
    private void Update()
    {
        if (player.activeSelf)
        {
            TouchActions();
            GameOver.SetActive(false);
        }
        else GameOver.SetActive(true);
    }
    private void TouchActions()
    { 
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    Destroyer.SetActive(true);
                    startPosition = touch.position;
                    break;
                case TouchPhase.Ended:
                    if(PlayerPrefs.GetString("Music")!="no") audio.Play();
                    endPosition = touch.position;
                    if (startPosition == endPosition) player.transform.position += new Vector3(0, 0, 1);
                    if (Mathf.Abs(endPosition.x - startPosition.x) > Mathf.Abs(endPosition.y - startPosition.y))
                    {
                        if (endPosition.x - startPosition.x > 0 && player.transform.position.x < rightLimit) player.transform.position += Vector3.right;
                        else if (endPosition.x - startPosition.x < 0 && player.transform.position.x > leftLimit) player.transform.position += Vector3.left;
                    }
                    else
                    {
                        if (endPosition.y - startPosition.y < 0 && player.transform.position.z > downLimit) player.transform.position += new Vector3(0, 0, -1);
                    }
                    break;
            }
        }
    }
}
