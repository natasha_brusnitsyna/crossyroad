﻿using UnityEngine;
using UnityEngine.UI;

public class CoinCollector : MonoBehaviour
{
    [SerializeField] Text coinScore;
    private int coins;
    private void Start()
    {
        coins = PlayerPrefs.GetInt("Coins");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Coin"))
        {
            coins++;
            PlayerPrefs.SetInt("Coins", coins);
        }
    }
    private void Update()
    {
        coinScore.text = PlayerPrefs.GetInt("Coins").ToString();
    }
}
