﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePool : MonoBehaviour
{
    [SerializeField] GameObject movingObject;
    [SerializeField] GameObject SpawnPosition;
    [SerializeField] int listCount;
    [SerializeField] float frequencyAppearance; 
    private GameObject CarContainer;
    private List<GameObject> list = new List<GameObject>();

    private void OnCollisionEnter(Collision collision)
    {
        collision.transform.position = SpawnPosition.transform.position;
        collision.gameObject.SetActive(false);
    }

    private void Start()
    {
        CarContainer = GameObject.FindGameObjectWithTag("Container");
        StartCoroutine(Spawn());
    }

    private GameObject GetObject()
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (!list[i].activeInHierarchy)
            {
                return list[i];
            }
        }
        return null;
    }

    private void Update()
    {
        if (GetObject() != null) GetObject().SetActive(true);
    }

    IEnumerator Spawn()
    {
        for (int i = 0; i < listCount; i++)
        {
            yield return new WaitForSeconds(frequencyAppearance);
            GameObject prefab = Instantiate(movingObject, SpawnPosition.transform.position, Quaternion.identity, CarContainer.transform);
            list.Add(prefab);
            prefab.SetActive(false);
        }
    }
}
