﻿using UnityEngine;

public class CoinController : MonoBehaviour
{
    [SerializeField] GameObject coin;
    [SerializeField] int coinChance;

    private void Start()
    {
        gameObject.SetActive(Random.Range(1, 101) <= coinChance);
        gameObject.transform.position = new Vector3(Random.Range(-3, 3), coin.transform.position.y, coin.transform.position.z);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
