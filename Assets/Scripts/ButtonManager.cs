﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] Image musicOn, musicOff;
    private void Start()
    {
        if (PlayerPrefs.GetString("Music") != "no")
        {
            transform.GetComponent<Image>().sprite = musicOn.sprite;
        }
        else
        {
            transform.GetComponent<Image>().sprite = musicOff.sprite;
        }
    }
    public void onPlayClick()
    {
        SceneManager.LoadScene("Main");
    }
    public void onMusicClick()
    {
        if (PlayerPrefs.GetString("Music") != "no")
        {
            PlayerPrefs.SetString("Music", "no");
            transform.GetComponent<Image>().sprite = musicOff.sprite;
        }
        else
        {
            PlayerPrefs.DeleteKey("Music");
            transform.GetComponent<Image>().sprite = musicOn.sprite;
        }
    }
}
