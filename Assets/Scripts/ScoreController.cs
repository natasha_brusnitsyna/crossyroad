﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    public int score;
    [SerializeField] Text scoreText;
    [SerializeField] Text highScore;
    private GameObject player;
    private Vector3 oldPosition;

    private void Start()
    {
        ScoreReset();
        player = GameObject.FindGameObjectWithTag("Player");
    }
    public void ScoreReset()
    {
        score = 0;
        oldPosition = new Vector3(0, 0, -1);
        scoreText.text = score.ToString();
    }
    private void Update()
    {
        if(player.transform.position.z > oldPosition.z)
        {
            oldPosition.z = player.transform.position.z;
            score++;
            scoreText.text = score.ToString();
            HighScore();
        }
    }
    private void HighScore()
    {
        if(PlayerPrefs.GetInt("Score")<score) PlayerPrefs.SetInt("Score", score);
        highScore.text = PlayerPrefs.GetInt("Score").ToString();
    }
}
