﻿using UnityEngine;
public class TreeGenerator : MonoBehaviour
{
    const int MAX_TREES_AMOUNT = 4;
    const int MIN_CAMERA_LIM = -4;
    const int MAX_CAMERA_LIM = 4;

    [SerializeField] GameObject tree;
    private int treeAmount;

    private void Start()
    {
        treeAmount = Random.Range(0, MAX_TREES_AMOUNT);
        for(int i = 0; i<treeAmount; i++)
        {
            tree.transform.position = new Vector3(Random.Range(MIN_CAMERA_LIM, MAX_CAMERA_LIM), 0, transform.position.z);
            Instantiate(tree, tree.transform.position, Quaternion.identity, transform);
        }
    }
}
