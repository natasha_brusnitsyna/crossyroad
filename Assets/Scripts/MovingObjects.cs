﻿using UnityEngine;
using DG.Tweening;

public class MovingObjects : MonoBehaviour
{
    [SerializeField] float maxSpeed;
    [SerializeField] float minSpeed;

    void Update()
    {
        //transform.DOMoveX(-20f, Random.Range(minSpeed, maxSpeed));
        transform.Translate(Vector3.left * Time.deltaTime * Random.Range(minSpeed, maxSpeed));
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag.Equals("Player") && gameObject.tag.Equals("Car"))
        {
            collision.gameObject.SetActive(false);
        }
    }
}
